{-# LANGUAGE OverloadedStrings #-}
module ParseComments where


import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import qualified Data.Map as M

parseComments :: IO (M.Map Int Text)
parseComments = do
    contents <- TIO.readFile "presentacion.md"
    return (parseText contents)


parseText :: Text -> M.Map Int Text
parseText = M.fromList . map addSlides . findSlides 1 . T.lines
    where
        findSlides :: Int -> [Text] -> [(Int, Text)]
        findSlides _ []     = []
        findSlides n (x:xs) = if "##" `T.isPrefixOf` x && not ("###" `T.isPrefixOf` x) then (n, findComment xs) : findSlides (n + 1) xs
                                else findSlides n xs
        
        findComment :: [Text] -> Text
        findComment = concatWith " " . takeWhile checkEnd . drop 1 . dropWhile (/="<!---") . checkNoComment

        checkEnd :: Text -> Bool
        checkEnd s = if "###" `T.isPrefixOf` s then True
                        else s /= "-->" && not ("##" `T.isPrefixOf` s) && not ("#" `T.isPrefixOf` s)

        checkNoComment :: [Text] -> [Text]
        checkNoComment [] = []
        checkNoComment (x:xs) = if "###" `T.isPrefixOf` x then x : checkNoComment xs
                                    else if "##" `T.isPrefixOf` x || "#" `T.isPrefixOf` x then []
                                        else x : checkNoComment xs

        
        concatWith :: Text -> [Text] -> Text
        concatWith _ [] = ""
        concatWith t (x:xs) = x `T.append` t `T.append` concatWith t xs


        addSlides :: (Int, Text) -> (Int, Text)
        addSlides (n, c) = (n + 2, c)
