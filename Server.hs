{-# LANGUAGE OverloadedStrings #-}
module Server (initWebServer) where


import qualified Data.Text.Lazy as LT
import qualified Data.Text as T
import Web.Scotty
import qualified Graphics.UI.Gtk as GTK
import Control.Monad.Trans (liftIO)
import Control.Monad (liftM)
import qualified Text.Blaze.Html5 as H
import Text.Blaze.Html5 ((!))
import Text.Blaze.Html5.Attributes as A
import Text.Blaze.Html.Renderer.Text (renderHtml)
import Network.Wai.Middleware.Static
import Network.Wai.Middleware.HttpAuth
import Network.Wai.Middleware.RequestLogger
import qualified Data.ByteString.Lazy as BSL
import qualified Data.ByteString as BS
import qualified Data.ByteString.Unsafe as BSU
import Data.Word
import Data.IORef
import qualified Data.Map as M
import System.IO.Error (tryIOError)
import Control.Concurrent (threadDelay)
import Control.Monad.Loops (whileM_)


import Paths_haskell_pdf_presenter

import Types





initWebServer :: State -> (State -> [GTK.Modifier] -> String -> IO Bool) -> IO ()
initWebServer state handleKey = scotty 3000 $ do

    -- get the static file folder


    zepto_min_js    <- liftIO $ getDataFileName "static/zepto.min.js"
    custom_js       <- liftIO $ getDataFileName "static/custom.js"
    base_min_css    <- liftIO $ getDataFileName "static/base-min.css"
    grids_min_css   <- liftIO $ getDataFileName "static/grids-min.css"
    custom_css      <- liftIO $ getDataFileName "static/custom.css"
    buttons_min_css <- liftIO $ getDataFileName "static/buttons-min.css"
    gesture_js      <- liftIO $ getDataFileName "static/gesture.js"
    touch_js        <- liftIO $ getDataFileName "static/touch.js"



    middleware $ staticPolicy $ only [ ("zepto.min.js", zepto_min_js)
                                     , ("custom.js", custom_js)
                                     , ("base-min.css", base_min_css)
                                     , ("grids-min.css", grids_min_css)
                                     , ("custom.css", custom_css)
                                     , ("buttons-min.css", buttons_min_css)
                                     , ("gesture.js", gesture_js)
                                     , ("touch.js", touch_js)
                                     ]
    middleware $ basicAuth (\u p -> return $ u == "felipe" && p == "canela") ""

    get "/" $ do
        html $ wrapper $ do
            H.div ! A.id "slides" ! A.class_ "pure-g-r" $ do
                -- we don't want to show the current slide on small screens like
                -- a phone
                H.div ! A.class_ "pure-u-3-5 pure-hidden-phone" $ do
                    H.img ! A.id "current" ! onerror "error_image_load()"


                H.div ! A.class_ "pure-u-2-5" $ do
                    H.img ! A.id "next" ! onerror "error_image_load()"
                        

            -- Hide this div on tablets and phones because in those devices we
            -- want to move with gestures and no with buttons
            {-H.div ! A.id "controls" ! A.class_ "pure-g pure-hidden-tablet pure-hidden-phone" $ do-}
                {-H.div ! A.class_ "pure-u-2-5" $ do-}
                    {-H.button ! A.id "back" ! A.class_ "pure-u-2-5 pure-button" $ "Back"-}

                {-H.div ! A.class_ "pure-u-2-5" $ do-}
                    {-H.button ! A.id "go" ! A.class_ "pure-u-2-5 pure-button" $ "Next"-}

                {-H.div ! A.id "goto" ! A.class_ "pure-u-1-5 "$ do-}
                    {-H.input ! type_ "text" ! class_ "" ! name "numberSlide"-}
                    {-H.button ! A.id "goto" ! class_ "pure-button" $ "Goto"-}

            H.div ! A.id "information" ! A.class_ "pure-g" $ do
                H.div ! A.class_ "pure-u-2-5" $  do
                    H.div ! A.id "counter" $ ""
                H.div ! A.class_ "pure-u-2-5" $ do
                    H.div ! A.id "slide-number" $ ""
            H.div ! A.class_ "" $ do
                H.div ! A.id "comment" $ ""
                    

    get "/current_slide.png" $ do
        (currentSlide, _) <- liftIO $ getStateSlides state

        current <- liftIO $ do
            let name = "/tmp/slide-big-" ++ show currentSlide ++ ".png"

            try <- tryIOError (BSL.readFile name)
            result <- newIORef try

            whileM_ (liftM isLeft . readIORef $ result) $ do
                threadDelay 50000
                try <- tryIOError (BSL.readFile name)
                writeIORef result try
            
            r <- readIORef result
            case r of
                Left _  -> return ""
                Right x -> return x


        raw current
        setHeader "Content-Type" "image/png"


    get "/next_slide.png" $ do
        (currentSlide, lastSlide) <- liftIO $ getStateSlides state

        if currentSlide + 1 > lastSlide then raw ""
            else do
                next <- liftIO $ BSL.readFile ("/tmp/slide--" ++ show (currentSlide + 1) ++ ".png")
                raw next
                setHeader "Content-Type" "image/png"



    -- Go to the next slide
    put "/go" $ do
        success <- liftIO $ handleKey state [] "right"

        if success then text $ "ok" 
            else text $ "error"

    -- Go to prev slide
    put "/back" $ do
        success <- liftIO $ handleKey state [] "left"
        if success then text $ "ok" 
            else text $ "error"

    -- Get a JSON with the state of the presenter like current slide, time, etc
    get "/state" $ do
        startTime                 <- liftIO $ readIORef (startTime state)
        warningTime               <- liftIO $ readIORef (warningTime state)
        endTime                   <- liftIO $ readIORef (endTime state)
        timer                     <- liftIO $ readIORef (timer state)
        clock                     <- liftIO $ readIORef (clock state)
        (currentSlide, lastSlide) <- liftIO $ getStateSlides state

        let stateJson = StateJson startTime warningTime endTime timer clock currentSlide lastSlide

        json $ stateJson
        

    get "/comment" $ do
        (currentSlide, _) <- liftIO $ getStateSlides state

        let comment = M.lookup currentSlide (comments state)

        case comment of
            Nothing -> html "No comments"
            Just x  -> html . LT.pack . T.unpack $ x


wrapper :: H.Html -> LT.Text
wrapper content' = renderHtml
    $ H.html $ do
        H.header $ do
            -- Zepto is a jquery like js library but more ligth
            H.script ! type_ "text/javascript" ! src "/zepto.min.js" $ ""
            H.script ! type_ "text/javascript" ! src "/gesture.js" $ ""
            H.script ! type_ "text/javascript" ! src "/touch.js" $ ""
            H.script ! type_ "text/javascript" ! src "/custom.js" $ ""

            -- base and grids are files of the pure css framework
            H.link ! type_ "text/css" ! rel "stylesheet" ! href "/base-min.css"
            H.link ! type_ "text/css" ! rel "stylesheet" ! href "/grids-min.css"
            H.link ! type_ "text/css" ! rel "stylesheet" ! href "/custom.css"
            H.link ! type_ "text/css" ! rel "stylesheet" ! href "/buttons-min.css"
        H.body content'


convert :: BSL.ByteString -> Either String BS.ByteString
convert img = undefined


getStateSlides :: State -> IO (Int, Int)
getStateSlides state = do
    currentSlide <- liftM round $ pageAdjustment state `GTK.get` GTK.adjustmentValue
    lastSlide    <- liftM round $ pageAdjustment state `GTK.get` GTK.adjustmentUpper
    return (currentSlide, lastSlide)



isLeft :: Either a b -> Bool
isLeft (Left _)  = True
isLeft (Right _) = False
