Zepto(function($){

    // we go to the next slide if the user click on the Go button
    $("#go").on("click", function(e){
        go();
    });

    $("#back").on("click", function(e){
        back();
    });


    $(document).swipeRight(function(){
        back();
    });

    //If the user slide the finger in the screen to the right, we go to the next
    //slide
    $(document).swipeLeft(function(){
        go();
    });


    getState();
    refreshImage();

});



function go(){
    $.ajax({
        type: "PUT",
        url: "/go",
        success: function(data){
            getState();
            refreshImage();
        },
        error: function(xhr, type){
            alert("Error sending request!");
        }
    });
}


function back(){
    $.ajax({
        type: "PUT",
        url: "/back",
        success: function(data){
            getState();
            refreshImage();
        },
        error: function(xhr, type){
            alert("Error sending request!");
        }
    });
}


function getState()
{
    $.ajax({
        type: "GET",
        url: "/state",
        success: function(data){
            setValsState(data);
        },
        error: function(xhr, type){
            alert("Can't get state");
        }
    });

    $.ajax({
        type: "GET",
        url: "/comment",
        success: function(data){
            $("#comment").html(data);
        },
        error: function(xhr, type){
            alert("Can't get the comment of the slide");
        }
    });
}


function setValsState(data){

    var timer = data["timer"];

    setSlidesNumbers(data["currentSlide"], data["totalSlides"]); 

    showTimer(data);

}



function setSlidesNumbers(current, total){
    $("#slide-number").text(current + "/" + total);
}


function refreshImage(){
    var timestamp = new Date().getTime();
    $("#current").attr("src", "/current_slide.png?" + timestamp);
    $("#next").attr("src", "/next_slide.png?" + timestamp);
}

function error_image_load()
{
    refreshImage();
}

function showTimer(data){

    if(data["timer"]["tag"] === "Stopped"){
        window.delta = data["timer"]["contents"][2] / 1000;
        var stopped = true;
        var updateTime = true;

    }else{
        var updateTime = false;

        if(window.delta === undefined){
            var date = new Date();
            var date2 = new Date(data["timer"]["contents"][1] / 1000);

            window.delta = date2 - date;
            updateTime = true;
        }


       var stopped = false;
    }


    if(updateTime){
        window.days    = Math.floor((window.delta / 1000) / 86400);
        window.hours   = Math.floor((window.delta / 1000) / 3600) % 24;
        window.minutes = Math.floor((window.delta / 1000) / 60) % 60;
        window.seconds = delta % 60;

        var str = toZeroNumber(window.minutes) + ":" + toZeroNumber(window.seconds);
        $("#counter").text(str);
    }

    if(!stopped){

        if(window.intervalClock === undefined){
            window.intervalClock = window.setInterval(function(){

                var str = toZeroNumber(window.minutes) + ":" + toZeroNumber(window.seconds);
                $("#counter").text(str);

                window.seconds = window.seconds - 1;

                if(window.seconds === -1){
                    window.seconds = 59;
                    window.minutes = window.minutes - 1;
                }


                if(window.minutes < 0)
                {
                    $("#counter").removeClass();
                    $("#counter").addClass('negative');
                }else if(window.minutes < 5){
                    $("#counter").removeClass();
                    $("#counter").addClass('warning');
                }


            }, 1000);
        }
    }
}

function toZeroNumber(n)
{
    if(n < 10 && n > -10)
    {
        if(n < 0)
        {
            return '-0' + Math.abs(n);
        }else{
            return '0' + n;
        }
    }else{
        return n.toString();
    }
}
