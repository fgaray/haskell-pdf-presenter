{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
module Types where


import Data.IORef
import Graphics.UI.Gtk.Poppler.Document
import Graphics.UI.Gtk
import Graphics.Rendering.Cairo
import qualified Data.Map as Map
import qualified Data.ByteString.Lazy as BSL
import Data.Aeson
import Data.Aeson.TH
import Data.Text (Text)


data TimerState =
  Counting Integer{-starting microseconds-} Integer{-ending microseconds-} |
  Stopped Bool{-soft pause-} Integer{-elapsed microseconds-} Integer{-remaining microseconds-}
data VideoMuteState = MuteOff | MuteBlack | MuteWhite deriving (Eq)
data ClockState = RemainingTime | ElapsedTime | WallTime12 | WallTime24

data State = State
-- Settings that are used only at program initialization
 { initSlide :: IORef Int
 , initPreviewPercentage :: IORef Int
 , initPresenterMonitor :: IORef Int
 , initAudienceMonitor :: IORef Int
 , initTimerMode :: IORef (TimerState -> IO TimerState)

-- Settable on command line
 , startTime :: IORef Integer{-microseconds-}
 , warningTime :: IORef Integer{-microseconds-}
 , endTime :: IORef Integer{-microseconds-}
 , documentURL :: IORef (Maybe String)
 , compression :: IORef Int

-- Dynamic State
 , timer :: IORef TimerState -- 'Maybe' for "dont" display or haven't started counting yet? Or as a seperate flag?
 , clock :: IORef ClockState
 , videoMute :: IORef VideoMuteState
 , fullscreen :: IORef Bool
 , mouseTimeout :: IORef Integer
 , history :: IORef ([Int]{-back-}, [Int]{-forward-})

-- UI State
 , builder :: Builder

-- Render State
 , views :: IORef (Map.Map Widget (Int{-width-}, Int{-height-}))
 , document :: IORef (Maybe Document)
 , pages :: IORef (Map.Map (Int{-page number-}, Int{-width-}, Int{-height-})
                          (BSL.ByteString, Int{-width-}, Int{-height-}, Int{-stride-}))
 , pageAdjustment :: Adjustment
 , comments :: Map.Map Int Text
}


data StateJson = StateJson { startTimeSt    :: Integer
                           , warningTimeSt  :: Integer
                           , endTimeSt      :: Integer
                           , timerSt        :: TimerState
                           , clockSt        :: ClockState
                           , currentSlideSt :: Int
                           , totalSlidesSt    :: Int
                           }

$(deriveToJSON defaultOptions{fieldLabelModifier = reverse . drop 2 . reverse}  ''StateJson)
$(deriveToJSON defaultOptions ''TimerState)
$(deriveToJSON defaultOptions ''ClockState)
